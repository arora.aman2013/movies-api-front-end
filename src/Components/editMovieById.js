import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import axios from 'axios'

const url = 'http://localhost:8000/api/movies/'

class AddMovie extends Component {
    state = {
        movie: []
    }
    componentDidMount() {
        console.log(url+this.props.params.id)
        axios(url + this.props.params.id).then(res => this.setState({movie: res.data}, console.log(res.data)));
    }
    onChange = (e) => {
        // let movieDetailsChange = {[e.target.name]: e.target.value}
        // console.log(movieDetailsChange)
        const title = e.target.name
        const value = e.target.value
        this.setState((prevState) => {
            let movie = {}
            Object.assign(movie, prevState.movie)
            movie[title] = value
            console.log(movie)
            prevState.movie = movie
            return prevState.movie
        }) 
    }
    onSubmit = (e) => {
        e.preventDefault()

        const newMovie = JSON.stringify(this.state.movie)
        console.log(newMovie)
        console.log(this.state.movie)
        fetch(url+this.props.params.id, {
            method:'PUT',
            headers: {
            'Content-Type': 'application/json',
            },
            mode: 'cors', 
            body: newMovie
        })
        .then((res) =>{
            if(res.ok)
                this.props.history.push('/movies')
                return res.json()    
        }).then((datajson ) => {
            this.props.history.push('/movies'); 
        }); 
    }
    render() {
        return (
            <div style = {divStyle}>
                <form style = {formStyle} onChange = {this.onChange} onSubmit = {this.onSubmit}>
                    <div style = {labelInputStyle}>
                        <label>Rank</label>
                        <input type = 'text' placeholder = 'Rank' name = 'rank' value = {this.state.movie.rank}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Title</label>
                        <input type = 'text' placeholder = 'Title' name = 'title' value = {this.state.movie.title}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Description</label>
                        <input type = 'text' placeholder = 'Description' name = 'description' value = {this.state.movie.description}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Runtime</label>
                        <input type = 'text' placeholder = 'Runtime' name = 'runtime' value = {this.state.movie.runtime}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Genre</label>
                        <input type = 'text' placeholder = 'Genre' name = 'genre' value = {this.state.movie.genre}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Rating</label>
                        <input type = 'text' placeholder = 'Rating' name = 'rating' value = {this.state.movie.rating}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Metascore</label>
                        <input type = 'text' placeholder = 'Metascore' name = 'metascore' value = {this.state.movie.metascore}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Votes</label>
                        <input type = 'text' placeholder = 'Votes' name = 'votes' value = {this.state.movie.votes}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Gross Earning</label>
                        <input type = 'text' placeholder = 'Gross Earning' name = 'gross_earning_mil' value = {this.state.movie.gross_earning_mil}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Director Id</label>
                        <input type = 'text' placeholder = 'Director Id' name = 'director_id' value = {this.state.movie.director_id}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Actor</label>
                        <input type = 'text' placeholder = 'Actor' name = 'actor' value = {this.state.movie.actor}/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Year</label>
                        <input type = 'year' placeholder = 'Year' name = 'year' value = {this.state.movie.year}/>
                    </div>
                    <button>Add Movie</button>
                </form>
            </div>
        )
    }
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

const formStyle = {
    width: '30%',
    backgroundColor: 'grey',
    padding: '2%'
}

const labelInputStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '1%'
}

export default withRouter (AddMovie)
