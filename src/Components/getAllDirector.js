import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import GetDirectors from './getDirectors'

const url = 'http://localhost:8000/api/directors/'

class getDirector extends Component {
    state = {
        directors: []
    }
    deleteDirector = (id) => {
        // console.log(url+id)
        axios((url + id), {method: 'delete'}).then(res => this.componentDidMount())
    }
    editDirector = (id) => {
        console.log(id)
    }
    componentDidMount() {
        axios(url).then(res => this.setState({directors: res.data}));
    }
    render() {
        return (
            <div>
                <div style = {{display: 'flex', justifyContent: 'center'}}>
                    <button style = {{height: '5vh', width: '10vw'}}><Link to = '/directors/add' style = {{textDecoration: 'none'}} >Add New director</Link></button>
                </div>
                <div style ={display}>
                    {this.state.directors.map((director => (
                        <GetDirectors key = {director.id} director = {director} deleteDirector = {this.deleteDirector} editDirector = {this.editDirector} />
                    )))}
                </div>
            </div>
        )
    }
}

const display = {
    display: 'flex',
    flexWrap: 'wrap'
}

export default getDirector
