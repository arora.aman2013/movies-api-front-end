import React, { Component } from 'react'
import GetMovies from './getMovies'
import axios from 'axios'
import { Link } from 'react-router-dom'

const url = 'http://localhost:8000/api/movies/'

class getAllMovie extends Component {
    state = {
        movies: []
    }

    componentDidMount() {
        axios(url).then(res => this.setState({movies: res.data}));
        console.log(url)
    }
    deleteMovie = (id) => {
        // console.log(url+id)
        axios((url + id), {method: 'delete'}).then(res => this.componentDidMount())
    }
    editMovie = (id) => {
        console.log(id)
    }
    render() {
        return (
            <div >
                <div style = {{display: 'flex', justifyContent: 'center'}}>
                    <button style = {{height: '5vh', width: '10vw'}}><Link to = '/movies/add' style = {{textDecoration: 'none'}} >Add New Movie</Link></button>
                </div>
                <div style ={display}>
                    {this.state.movies.map((movie => (
                        <GetMovies key = {movie.id} movie = {movie} deleteMovie = {this.deleteMovie} editMovie = {this.editMovie} />
                    )))}
                </div>
            </div>
        )
    }
}

const display = {
    display: 'flex',
    flexWrap: 'wrap'
}

export default getAllMovie
