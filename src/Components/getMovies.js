import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class getMovies extends Component {
    editButton = (e) => {
        this.props.history.push(`/movies/edit/${this.props.movie.id}`)
    }
    deleteButton = (e) => {
        console.log(e.target);
    }
    render() {
        return (
            <div style = {movieStyle}>
                <h3 style = {{marginBottom: '1%'}}>{this.props.movie.title}</h3>
                <p>{this.props.movie.description}</p>
                <div style={displayFlex}>
                    <p>Rank: {this.props.movie.rank}</p>
                    <p>Runtime: {this.props.movie.runtime}</p>
                    <p>Genre: {this.props.movie.genre}</p>
                </div>
                <div style={displayFlex}>
                    <p>Rating: {this.props.movie.rating}</p>
                    <p>Metascore: {this.props.movie.metascore}</p>
                    <p>Votes: {this.props.movie.votes}</p>
                </div>
                <div style = {displayFlex}>
                    <p>Gross Earning: {this.props.movie.gross_earning_mil}</p>
                    <p>Actor: {this.props.movie.actor}</p>
                    <p>Year: {this.props.movie.year}</p>
                </div>
                <div style = {displayFlex}>
                    <button style = {buttonStyle} onClick = {this.editButton}>Edit</button>
                    <button style = {buttonStyle} onClick = {this.props.deleteMovie.bind(this, this.props.movie.id)}>Delete</button>
                </div>
            </div>
        )
    }
}

const displayFlex = {
    marginTop: '2%',
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
}

const buttonStyle = {
    height: '5vh',
    width: '5vw',
    cursor: 'pointer'
}
const movieStyle = {
    width: '40%',
    margin: '2.5%',
    backgroundColor: 'grey',
    padding: '2%',
}
export default withRouter(getMovies)
