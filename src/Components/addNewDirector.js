import React, { Component } from 'react'

class AddMovie extends Component {
    state = {
        name: '',
    }
    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }
    onSubmit = (e) => {
        e.preventDefault()
        const newMovie = JSON.stringify(this.state)
        console.log(newMovie)
        console.log(this.state)
        fetch('http://localhost:8000/api/directors/', {
            method:'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            mode: 'cors', 
            body: newMovie
        })
        .then((res) =>{
            if(res.ok)
                return res.json()       
            })
        .then((datajson ) => {
            console.log(datajson);
            this.props.history.push('/directors'); 
        }); 
    }
    render() {
        return (
            <div style = {divStyle}>
                <form style = {formStyle} onChange = {this.onChange} onSubmit = {this.onSubmit}>
                    <div style = {labelInputStyle}>
                        <label>Name</label>
                        <input type = 'text' placeholder = 'Name' name = 'name'/>
                    </div>
                    <button>Add Movie</button>
                </form>
            </div>
        )
    }
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

const formStyle = {
    width: '30%',
    backgroundColor: 'grey',
    padding: '2%'
}

const labelInputStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '1%'
}

export default AddMovie
