import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import axios from 'axios'

const url = 'http://localhost:8000/api/directors/'

class Adddirector extends Component {
    state = {
        director: []
    }
    componentDidMount() {
        console.log(url+this.props.params.id)
        axios(url + this.props.params.id).then(res => this.setState({director: res.data}, console.log(res.data)));
    }
    onChange = (e) => {
        // let directorDetailsChange = {[e.target.name]: e.target.value}
        // console.log(directorDetailsChange)
        const title = e.target.name
        const value = e.target.value
        this.setState((prevState) => {
            let director = {}
            Object.assign(director, prevState.director)
            director[title] = value
            console.log(director)
            prevState.director = director
            return prevState.director
        }) 
    }
    onSubmit = (e) => {
        e.preventDefault()
        const newdirector = JSON.stringify(this.state)
        console.log(newdirector)
        console.log(this.state)
        fetch('http://localhost:8000/api/directors/'+this.props.params.id, {
            method:'PUT',
            headers: {
            'Content-Type': 'application/json',
            },
            mode: 'cors', 
            body: newdirector
        })
        .then((res) =>{
            if(res.ok)
                return res.json()       
            })
        .then((datajson ) => {
            console.log(datajson);
            this.props.history.push('/directors'); 
        }); 
    }
    render() {
        return (
            <div style = {divStyle}>
                <form style = {formStyle} onChange = {this.onChange} onSubmit = {this.onSubmit}>
                    <div style = {labelInputStyle}>
                        <label>Name</label>
                        <input type = 'text' placeholder = 'Name' name = 'name' value = {this.state.director.name}/>
                    </div>
                    <button>Add director</button>
                </form>
            </div>
        )
    }
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

const formStyle = {
    width: '30%',
    backgroundColor: 'grey',
    padding: '2%'
}

const labelInputStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '1%'
}

export default withRouter(Adddirector)
