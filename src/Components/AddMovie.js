import React, { Component } from 'react'

class AddMovie extends Component {
    state = {
        rank: '',
        title: '',
        description: '',
        runtime: '',
        genre: '',
        rating: '',
        metascore: '',
        votes: '',
        gross_earning_mil: "",
        director_id: '',
        actor: "",
        year: ''
    }
    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }
    onSubmit = (e) => {
        e.preventDefault()
        const newMovie = JSON.stringify(this.state)
        console.log(newMovie)
        console.log(this.state)
        fetch('http://localhost:8000/api/movies/', {
            method:'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            mode: 'cors', 
            body: newMovie
        })
        .then((res) =>{
            if(res.ok)
                return res.json()       
            })
        .then((datajson ) => {
            console.log(datajson);
            this.props.history.push('/movies'); 
        }); 
    }
    render() {
        return (
            <div style = {divStyle}>
                <form style = {formStyle} onChange = {this.onChange} onSubmit = {this.onSubmit}>
                    <div style = {labelInputStyle}>
                        <label>Rank</label>
                        <input type = 'text' placeholder = 'Rank' name = 'rank'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Title</label>
                        <input type = 'text' placeholder = 'Title' name = 'title'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Description</label>
                        <input type = 'text' placeholder = 'Description' name = 'description'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Runtime</label>
                        <input type = 'text' placeholder = 'Runtime' name = 'runtime'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Genre</label>
                        <input type = 'text' placeholder = 'Genre' name = 'genre'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Rating</label>
                        <input type = 'text' placeholder = 'Rating' name = 'rating'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Metascore</label>
                        <input type = 'text' placeholder = 'Metascore' name = 'metascore'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Votes</label>
                        <input type = 'text' placeholder = 'Votes' name = 'votes'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Gross Earning</label>
                        <input type = 'text' placeholder = 'Gross Earning' name = 'gross_earning_mil'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Director Id</label>
                        <input type = 'text' placeholder = 'Director Id' name = 'director_id'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Actor</label>
                        <input type = 'text' placeholder = 'Actor' name = 'actor'/>
                    </div>
                    <div style = {labelInputStyle}>
                        <label>Year</label>
                        <input type = 'year' placeholder = 'Year' name = 'year'/>
                    </div>
                    <button>Add Movie</button>
                </form>
            </div>
        )
    }
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

const formStyle = {
    width: '30%',
    backgroundColor: 'grey',
    padding: '2%'
}

const labelInputStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '1%'
}

export default AddMovie
