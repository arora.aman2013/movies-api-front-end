import React, {Component} from 'react'
import { Link } from 'react-router-dom'

class Header extends Component{
    render(){
        return (
            <header style ={headerStyle}>
                <Link style = {linkStyle} to= "/movies">Movies</Link>
                <Link style = {linkStyle} to= "/directors">Director</Link>
            </header>
        )
    }
}

const headerStyle = {
    display: 'flex',
    justifyContent: 'space-evenly',
    color: 'white',
    textAlign: 'center',
    marginBottom: '2vh' 
}

const linkStyle = {
    color: 'white',
    textDecoration: 'none',
    fontSize: '2rem'
}

export default Header