import axios from 'axios'
import React, { Component } from 'react'

const url = 'http://localhost:8000/api/movies/'

class GetMovieById extends Component {
    state = {
        movie: []
    }
    componentDidMount() {
        console.log(url+this.props.params.id)
        axios(url + this.props.params.id).then(res => this.setState({movie: res.data}, console.log(this.state.movie)));
    }
    deleteMovie = (id) => {
        // console.log(url+id)
        axios((url + id), {method: 'delete'}).then(res => this.componentDidMount())
    }
    editMovie = (id) => {
    
    }
    render() {
        return (
            <div style = {movieStyle}>
                <h3 style = {{marginBottom: '1%'}}>{this.state.movie.title}</h3>
                <p>{this.state.movie.description}</p>
                <div style={displayFlex}>
                    <p>Rank: {this.state.movie.rank}</p>
                    <p>Runtime: {this.state.movie.runtime}</p>
                    <p>Genre: {this.state.movie.genre}</p>
                </div>
                <div style={displayFlex}>
                    <p>Rating: {this.state.movie.rating}</p>
                    <p>Metascore: {this.state.movie.metascore}</p>
                    <p>Votes: {this.state.movie.votes}</p>
                </div>
                <div style = {displayFlex}>
                    <p>Gross Earning: {this.state.movie.gross_earning_mil}</p>
                    <p>Actor: {this.state.movie.actor}</p>
                    <p>Year: {this.state.movie.year}</p>
                </div>
                <div style = {displayFlex}>
                    <button style = {buttonStyle}>Edit</button>
                    <button style = {buttonStyle}>Delete</button>
                </div>
            </div>
        )
    }
}

const displayFlex = {
    marginTop: '2%',
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
}

const buttonStyle = {
    height: '5vh',
    width: '5vw'
}
const movieStyle = {
    width: '40%',
    margin: '2.5%',
    backgroundColor: 'grey',
    padding: '2%',
}

export default GetMovieById
