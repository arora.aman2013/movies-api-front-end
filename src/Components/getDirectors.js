import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class getDirectors extends Component {
    editButton = (e) => {
        this.props.history.push(`/directors/edit/${this.props.director.id}`)
    }
    deleteButton = (e) => {
        console.log(e.target);
    }
    render() {
        return (
            <div style = {directorStyle}>
                <h3 style = {{marginBottom: '1%'}}>{this.props.director.name}</h3>
                <div style = {displayFlex}>
                    <button style = {buttonStyle} onClick = {this.editButton}>Edit</button>
                    <button style = {buttonStyle} onClick = {this.props.deleteDirector.bind(this, this.props.director.id)}>Delete</button>
                </div>
            </div>
        )
    }
}

const displayFlex = {
    marginTop: '2%',
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
}

const buttonStyle = {
    height: '5vh',
    width: '5vw',
    cursor: 'pointer'
}
const directorStyle = {
    width: '40%',
    margin: '2.5%',
    backgroundColor: 'grey',
    padding: '2%',
}
export default withRouter(getDirectors)
