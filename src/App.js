import React, { Component } from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import './App.css';
import Header from './Components/Header'
import GetAllMovie from './Components/getAllMovie'
import GetMovieById from './Components/GetMovieById'
import AddMovie from './Components/AddMovie'
import EditMovieById from './Components/editMovieById'
import GetAllDirector from './Components/getAllDirector'
import AddNewDirector from './Components/addNewDirector'
import EditDirectorById from './Components/editDirectorById'

class App extends Component {
  render() {
    return (
        <Router>
          <Header />
          <Switch>
            <Route path = '/movies/add' component = {AddMovie}/>
            <Route path = '/movies/edit/:id' render = {(props) => (
              <EditMovieById params = {props.match.params} />
            )} />   
            <Route path = '/movies/:id' render = {(props) => (
              <React.Fragment>
                <GetMovieById params = {props.match.params}/>
              </React.Fragment>
            )} />
            <Route path = '/movies' component = {GetAllMovie} />
            <Route path = '/directors/edit/:id' render = {(props) => (
              <EditDirectorById params = {props.match.params} />
            )} /> 
            <Route path = '/directors/add' component = {AddNewDirector}/>
            <Route path = '/directors' component = {GetAllDirector} />
          </Switch>
        </Router>
    )
  }
}

export default App
